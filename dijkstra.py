#cuidado algoritmo complejo
def ordenar(lista):
    "ordena una lista de numeros"
    for a in lista:
        lista[lista.index(a)] = int(a)
    n = len(lista)
    while n>=1:
        for i in range(1,n):
            if lista[i-1] > lista[i]:
                lista[i-1], lista[i] = lista[i], lista[i-1]
        n-=1
grafo={"a":[["b",3],["c",1]],"b":[["a",3],["d",1],["g",5]],"c":[["a",1],["d",2],["f",5]],"d":[["b",1],["c",2],["f",2],["e",4]],"e":[["d",4],["g",2],["h",1]],"f":[["c",5],["d",2],["h",3]],"g":[["b",5],["e",2]],"h":[["e",1],["f",3]]}
def dijkstra(grafo,inicio,fin=0):
    "devuelve las rutas mas corta"
    visitados=[inicio]
    pivote=inicio
    distancias={inicio:[[0,0,0]]}
    while pivote:
        for i in grafo[pivote]:
            if i[0] not in visitados:
                if i[0] not in distancias.keys():
                    distancias[i[0]]=[[distancias[pivote][0][0]+i[1],pivote,distancias[pivote][0][2]+1]]
                else:
                    for j in distancias[i[0]]:
                        if j[0]>distancias[pivote][0][0]+i[1]:
                            j=[distancias[pivote][0][0]+i[1],pivote,distancias[pivote][0][2]+1]
        lstemp=[]
        for i in distancias.keys():
            if i not in visitados:
                lstemp.append(distancias[i][0][0])
        ordenar(lstemp)
        visitados+=pivote
        for i in distancias.keys():
            if (distancias[i][0][0]==lstemp[0]) and (i != pivote) and (i not in visitados):
                pivote=i
            elif len(grafo.keys())==len(visitados):
                pivote=""
    print distancias
dijkstra(grafo,"a")
print "x:[0]=distancia absoluta(aristas) x:[1]=proximo nodo ha segui (de la ruta mas corta) X:[2]=distancia bruta (numero de iteraciones)"
