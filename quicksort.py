import random
def elegirpivote(lista,iprimer,iultimo):
    """esta funcion elige el mejor pivote"""
    vpivote=lista[iultimo]
    a=iprimer
    b=iultimo-1
    while a<b:
        if lista[a]>vpivote and lista[b]<vpivote:
            lista[a],lista[b] = lista[b],lista[a]
            a+=1
            b-=1
        elif lista[a]>vpivote:
            b-=1
        elif lista[b]<vpivote:
            a+=1
        else:
            a+=1
            b-=1
    lista[a],lista[iultimo] = lista[iultimo],lista[a]
    return a

def quicksort(lista,iprimer,iultimo):
    if iprimer<iultimo:
        pivote=elegirpivote(lista,iprimer,iultimo)
        quicksort(lista,iprimer,pivote-1)
        quicksort(lista,pivote+1,iultimo)
    return lista
lista=[]
for i in range(1,20000):
    lista.append(i)
random.shuffle(lista)    
print quicksort(lista,0,len(lista)-1)