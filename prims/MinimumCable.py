nodes = {}
nodes_connected = {}
node_to_connect = str
next_node = str
vecinos = {}
cable_min = float("inf")
cable_total = 0
conexions = []

def parsearTxt(f):    
    while True:
        try:
            s = f.readline()
            if s == '':
                break
            node = s[0]
            neighbor = s[2]
            
            nodss,aa = s.split(':')
            node,neighbor = nodss.split('-')
#            print node,neighbor
#            exit()
            for x in s:
                if x.isdigit():
                    a = x         
            route = int(s[s.find(":")+1:s.rfind(a)+1])
            
            if node in nodes.keys() and neighbor not in nodes.keys():
                nodes[neighbor] = {node: [route, False]}

            elif node not in nodes.keys():
                nodes[node] = {neighbor: [route, False]}
            
            elif neighbor not in nodes[node].keys():        
                nodes[node][neighbor] = [route, False]
                
        except:
            break
      
    f.close()
    while True:
        try:
            for u in nodes:
                for v in nodes[u]:
                    if v not in nodes and len(nodes[u].items()) > 0:
                        print "Gey"
                        nodes[v] = {u:nodes[u].pop(v)}
            break
        except:
            continue 
def findNeighbors():
    print "Nodos Conectados -> ", nodes_connected.viewkeys(), "\n"
    for u in nodes_connected:
        for v in nodes[u]:
            if u not in nodes_connected or v not in nodes_connected:
                if nodes[u][v][1] == False:                    
                    if u in vecinos:
                        for x in vecinos[u]:
                            if nodes[u][v][0] < vecinos[u][x]:
                                vecinos[u] = {v:nodes[u][v][0]}
                                break
                    else:
                        vecinos[u] = {v:nodes[u][v][0]}
        
        for v in nodes:
            if u in nodes[v]:
                if u not in nodes_connected or v not in nodes_connected:
                    if nodes[v][u][1] == False:
                        if v in vecinos:
                            for x in vecinos[v]:
                                if vecinos[v][x] > nodes[v][u][0]:
                                    vecinos[v] = {u:nodes[v][u][0]}
                        else:
                            vecinos[v] = {u:nodes[v][u][0]}
                        
                        
def connectNeighbors():
    global vecinos
    global cable_min
    global cable_total
    global conexions    
    print ("Posibles vecinos -> ", vecinos, "\n"    )
    for u in vecinos:
        for v in vecinos[u]:
            if u not in nodes_connected or v not in nodes_connected:
                if vecinos[u][v] < cable_min:
                    cable_min = nodes[u][v][0]
                    node_to_connect = u
                    next_node = v
             
    cable_total += cable_min
    
    if node_to_connect not in nodes_connected:
        nodes_connected[node_to_connect] = node_to_connect
        conexions.append(str(next_node) + " with " +str(node_to_connect))
        print "Nodo -",next_node, "- Conectado a nodo -", node_to_connect, "- Con un cable de ", cable_min
        
    else:
        nodes_connected[next_node] = next_node
        conexions.append(str(node_to_connect) + " with " +str(next_node))
        print "Nodo -",node_to_connect, "- Conectado a nodo -", next_node, "- Con un cable de ", cable_min

    nodes[node_to_connect][next_node][1] = True    
    vecinos = {}
    cable_min = float("inf")
    print "============================================================\n\n"    

def MinimumCable(f):    
    parsearTxt(f)

    for u in nodes:
        nodes_connected[u] = u
        break
    
    while True:    
        if nodes_connected.viewkeys() == nodes.viewkeys():
            break        
        findNeighbors()  
        connectNeighbors()
        
    for x in conexions:
        print x
    print "Total Cable:", cable_total

f = open("connections.txt")
MinimumCable(f)
