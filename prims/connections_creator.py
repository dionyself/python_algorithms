import string
from random import choice

conns   = []
lengths = range(1,15)
letters = string.uppercase[0:15]#range(1,100)
edgesRange   = range(50)


def get_connection():
    node1 = choice(letters)
    node2 = choice(letters)
    
    while (node1 == node2 or is_new(node1, node2) != True):
        node1 = choice(letters)
        node2 = choice(letters)
    
    return [node1, node2, choice(lengths)]
    

def is_new(n1, n2): 
    global conns
    
    for i in range(0, len(conns)):
        #print conns[i]
        if (n1 == conns[i][0] and n2 == conns[i][1]) \
        or (n1 == conns[i][1] and n2 == conns[i][0]):
            return False
            
    return True            


for i in edgesRange:
    node1 = choice(letters)
    node2 = choice(letters)
    
    conns.append(get_connection())
   
txt = ""
for j in range(0, len(conns)):
    txt += "%s-%s:%d\n" % tuple(conns[j])
    
myFile = open('connections.txt', 'w')
myFile.write(txt)
myFile.close()
#print conns
