txt     = open('connections.txt').read()

#parse text file and get all unique nodes
def parseData(txt):
    
    conns = {}
    for conn in txt.split('\n'):

        if conn:
            nodes,length = conn.split(':')
            nodes = nodes.split('-')
            
            if nodes[0] in conns:
                conns[nodes[0]][nodes[1]] = int(length)
            else:
                conns[nodes[0]] = {nodes[1] : int(length)}                
                
            
            if nodes[1] in conns:
                conns[nodes[1]][nodes[0]] = int(length)                
            else:
                conns[nodes[1]] = {nodes[0] : int(length)}
                
    return conns 
                                
            
            
def findPath(conns):

    path    = []
    queue   = [conns.iterkeys().next()]
    #ii = 1
    while len(queue) != len(conns.keys()):
        #print  ii
        #ii += 1
        tmpBest = None
        
        for sn in queue:
            for en,length in conns[sn].iteritems():
               
                if (tmpBest is None or tmpBest[2] > length) and en not in queue:
                    tmpBest = [sn,en,length]
        
        
        
        
        path.append(tmpBest)
        queue.append(tmpBest[1])
        
        
        del conns[tmpBest[0]][tmpBest[1]]
        del conns[tmpBest[1]][tmpBest[0]]

    return path


        
conns = parseData(txt)   
best  = findPath(conns)
total = 0

for path in best:
    total += path[2]
    print path
    
print "El total de cable usado es: %d" % total    
    
