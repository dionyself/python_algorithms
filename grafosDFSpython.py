grafo={"papa":["hijo","hija"],"hijo":["nieto1","nieto2","nieta1"],"hija":["nieta2","nieta3","nieto3"],"nieto1":"","nieto2":"","nieta1":"","nieta2":"","nieta3":"","nieto3":""}
visitado=[]
pila=[]
def visitarvertices(grafo,vertice):
    global visitado
    global pila
    pila.append(vertice)                 # apilar el nodo padre, para luego visitar los hijos
    for i in grafo[vertice]:
        if i != "":                      # si el nodo no esta vacio "ir mas profundo"
            visitarvertices(grafo,i)     # implementacion de DFS mediante recursividad 
    if pila != "":
        visitado.append(pila.pop(-1))    # luego de visitar todos los nodos hijos desapilamos el padre y lo visitamos!
visitarvertices(grafo,"papa")
print "el grafo ha sido recorrido, elementos no visitados: ",pila
print "elementos visitados: ",visitado
print "total de nodos: ", len(visitado)
        
        
    