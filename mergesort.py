import random
def mergesort(lista):
    n=len(lista)
    if n==1:
        return lista
    left = lista[0:int(n/2)]
    right = lista[int(n/2):]
    merge1 = mergesort(left)
    merge2 = mergesort(right)
    lista=merge(merge1,merge2)
    return lista
def merge(izquierda,derecha):
    salida=[]
    while len(izquierda)>0 or len(derecha)>0:
        if derecha and not izquierda:
            salida.append(derecha.pop(0))
        elif (izquierda and not derecha) or izquierda[0]<derecha[0]:
            salida.append(izquierda.pop(0))
        else:
            salida.append(derecha.pop(0))
    return salida
numeros=[]
for i in range(0,1000000):
    numeros.append(int(random.uniform(0,1000)))
print mergesort(numeros)
